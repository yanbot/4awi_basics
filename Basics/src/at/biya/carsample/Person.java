package at.biya.carsample;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class Person {
	private List<Car> cars;
	private String firstName;
	private String lastName;
	private LocalDate birthDate;
	
	public Person(String firstName, String lastName, LocalDate birthDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		
		cars = new ArrayList<Car>();
	}
	
	public List<Car> getCars() {
		return cars;
	}

	public void addCar(Car car) {
		cars.add(car);
	}
	
	public double getValueOfCars()
	{
		double value = 0;
		
		for(Car car : cars)
		{
			value += car.getPrice();
		}
		
		return value;
	}
	
	public int getAge() {
		return Period.between(this.birthDate, LocalDate.now()).getYears();
	}
}
