package at.biya.carsample;

import java.time.LocalDate;
import java.util.Date;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Engine e1 = new Engine("Diesel", 200);
		Manufacturer m1 = new Manufacturer("VW","Deutschland", 10);
		Car c1 = new Car("White", 150, 10000, 0.08, e1, m1, 100000);
		
		Engine e2 = new Engine("Benzin", 300);
		Manufacturer m2 = new Manufacturer("BMW","Deutschland", 5);
		Car c2 = new Car("Black", 150, 10000, 0.08, e2, m2, 100000);
		
		Person p1 = new Person("Yanboy", "Bitschboy", LocalDate.parse("2000-01-01"));
		Date today = new Date();

				
		p1.addCar(c1);
		p1.addCar(c2);
		
		for(Car car : p1.getCars())
		{
			System.out.println("Usage: " + car.getUsage());
			System.out.println("Type: " + car.getType());
			System.out.println("Price: " + car.getPrice());
			System.out.println();
		}
		
		System.out.println("Value of all Cars: " + p1.getValueOfCars());
		System.out.println("Age: " + p1.getAge());
		
	}

}
